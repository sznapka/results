import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LeaguesComponent} from './components/leagues/leagues.component';
import {RoundComponent} from './components/round/round.component';
import {MatchesComponent} from './components/matches/matches.component';
import {ScoreboardComponent} from './components/scoreboard/scoreboard.component';

const routes: Routes = [
  {path: '', component: LeaguesComponent},
  {path: 'league/:id/:edition/:round', component: RoundComponent},
  {path: 'league/:id/:edition/:round/', redirectTo: 'league/:id/:edition/:round'},
  {path: 'league/:id/:edition/:round/matches', component: MatchesComponent},
  {path: 'scoreboard/:id/:edition', component: ScoreboardComponent},
  {path: '*', redirectTo: '/'},
  {path: '**', redirectTo: '/'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

import {Injectable} from '@angular/core';
import {Group} from '../components/leagues/leagues.component';

@Injectable({
  providedIn: 'root'
})
export class ScoringService {

  constructor() {
  }

  public static getPointsForReversedScore(score: any, scoring: any): number {
    if (!score) {
      return 0;
    }
    if (score === 'wo') {
      return scoring['wo'] as number;
    }
    if (score === 'wo+') {
      return scoring['wo-'] as number;
    }
    if (score === 'wo-') {
      return scoring['wo+'] as number;
    }

    return scoring[score.split('').reverse().join('')] as number;
  }

  static calculatePoints(group: Group, results: object, scoring: object): Group {
    for (const player of group.players) {
      const playerRef = player.path;
      const points = [];
      player['wins'] = 0;
      player['sets'] = 0;
      player['walkovers'] = 0;
      player['matches'] = 0;

      for (const match of Object.keys(results)) {
        const resultParts = results[match].split(':');
        if (match.indexOf(playerRef) === 0) { // match saved as Player:Opponent
          points.push(scoring[results[match]]);
          if (resultParts.length === 2) {
            player['wins'] += (resultParts[0] > resultParts[1]) ? 1 : 0;
            player['sets'] += (resultParts[0] - resultParts[1]);
          }
          if (['wo-', 'wo'].indexOf(results[match]) >= 0) {
            player['walkovers']++;
          }
          player['matches']++;
        } else if (match.indexOf(playerRef) > 0) { // match saved as Opponent:Player
          points.push(ScoringService.getPointsForReversedScore(results[match], scoring));
          if (resultParts.length === 2) {
            player['wins'] += (resultParts[0] < resultParts[1]) ? 1 : 0;
            player['sets'] += (resultParts[1] - resultParts[0]);
          }
          if (['wo-', 'wo'].indexOf(results[match]) >= 0) {
            player['walkovers']++;
          }
          player['matches']++;
        }
      }
      let numberOfMatches = '_length' in scoring ? scoring['_length'] : points.length;
      const pointsSortedAndSliced = points.sort((a, b) => b - a).slice(0, numberOfMatches);
      player['points'] = pointsSortedAndSliced.reduce((a, b) => a + b, 0);
    }
    group.players.sort((a, b) => {
      if (a['points'] < b['points']) {
        return 1;
      }
      if (a['points'] > b['points']) {
        return -1;
      }

      if (a['wins'] < b['wins']) {
        return 1;
      }
      if (a['wins'] > b['wins']) {
        return -1;
      }

      if (a['sets'] < b['sets']) {
        return 1;
      }
      if (a['sets'] > b['sets']) {
        return -1;
      }
      return 0;
    });

    return group;
  }
}

import {Injectable} from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection, DocumentChangeAction, QueryFn} from 'angularfire2/firestore';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FirestoreService {

  constructor(private afs: AngularFirestore) {
  }

  collectionWithIds<T>(path: string, queryFn?: QueryFn): Observable<any[]> {
    return this.afs.collection<T>(path, queryFn)
      .snapshotChanges()
      .pipe(
        map((actions: DocumentChangeAction<T>[]) => {
          return actions.map((a: DocumentChangeAction<T>) => {
            const data: Object = a.payload.doc.data() as T;
            const id = a.payload.doc.id;
            return {id, ...data};
          });
        }),
      );
  }

  collection<T>(path: string, queryFn?: QueryFn): AngularFirestoreCollection<T> {
    return this.afs.collection<T>(path, queryFn);
  }

  doc<T>(path: string) {
    return this.afs.doc(path).valueChanges();
  }

  add(collection: string, doc: object) {
    return this.afs.collection(collection).add(doc);
  }

  update(path: string, data: any) {
    return this.afs.doc(path).update(data);
  }

  delete(path: string) {
    return this.afs.doc(path).delete();
  }
}

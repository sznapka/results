import {BrowserModule} from '@angular/platform-browser';
import {LOCALE_ID, NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LeaguesComponent} from './components/leagues/leagues.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AngularFirestoreModule} from 'angularfire2/firestore';
import {environment} from '../environments/environment';
import {AngularFireModule} from 'angularfire2';
import {DocPipe} from './pipes/doc.pipe';
import {ResultPipe} from './pipes/result.pipe';
import {AddScoreComponent} from './components/add-score/add-score.component';
import {LoginComponent} from './components/login/login.component';
import {AngularFireAuthModule} from 'angularfire2/auth';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {RoundComponent} from './components/round/round.component';
import {ShortenPipe} from './pipes/shorten.pipe';
import {PlayersComponent} from './modal/players/players.component';
import {MatchesComponent} from './components/matches/matches.component';
import {FromtimestampPipe} from './pipes/fromtimestamp.pipe';
import {registerLocaleData} from '@angular/common';
import localePl from '@angular/common/locales/pl';
import {SurnamePipe} from './pipes/surname.pipe';
import {GroupAbbrevPipe} from './pipes/group-abbrev.pipe';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {SettleRoundComponent} from './components/settle-round/settle-round.component';
import {ScoreboardComponent} from './components/scoreboard/scoreboard.component';

registerLocaleData(localePl);

@NgModule({
  declarations: [
    AppComponent,
    LeaguesComponent,
    DocPipe,
    ResultPipe,
    AddScoreComponent,
    LoginComponent,
    RoundComponent,
    ShortenPipe,
    PlayersComponent,
    MatchesComponent,
    FromtimestampPipe,
    SurnamePipe,
    GroupAbbrevPipe,
    SettleRoundComponent,
    ScoreboardComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule.forRoot(),
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp(environment.firebase),
    AppRoutingModule,
  ],
  providers: [
    {provide: LOCALE_ID, useValue: 'pl'}
  ],
  entryComponents: [
    PlayersComponent,
    SettleRoundComponent,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {FirestoreService} from '../../services/firestore.service';
import {Edition, League, Result, Round} from '../leagues/leagues.component';
import {AngularFireAuth} from 'angularfire2/auth';
import {User} from 'firebase';

@Component({
  selector: 'app-matches',
  templateUrl: './matches.component.html',
  styleUrls: ['./matches.component.css']
})
export class MatchesComponent implements OnInit {

  public user: User;
  public league: League;
  public edition: Edition;
  public round: Round;
  public results = [];

  constructor(private fs: FirestoreService, private route: ActivatedRoute, public afAuth: AngularFireAuth) {
  }

  ngOnInit() {
    this.afAuth.authState.subscribe(res => this.user = res);
    this.fs.doc<League>('leagues/' + this.route.snapshot.paramMap.get('id')).subscribe((res: League) => {
      this.edition = res.edition.filter(x => x.name === this.route.snapshot.paramMap.get('edition'))[0];
      this.league = {id: this.route.snapshot.paramMap.get('id'), ...res};
      this.round = this.edition.rounds.filter(x => x.name === this.route.snapshot.paramMap.get('round'))[0] as Round;
      for (const group of this.round.groupsCol) {
        this.fs.collectionWithIds('results', r => r.where('group', '==', group).orderBy('date', 'desc'))
          .subscribe(res2 => {
            this.results.push(...res2);
            this.results = this.results.sort((a, b) => b.date.seconds - a.date.seconds);
          });
      }
    });
  }

  removeResult(result: Result) {
    if (confirm('Napewno usunąć ten wynik?')) {
      this.results = this.results.filter(x => x['group']['path'] !== result.group['path']);
      this.fs.delete(`results/${result['id']}`);
    }
  }
}

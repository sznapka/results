import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {FirestoreService} from '../../services/firestore.service';
import {DocumentReference} from 'angularfire2/firestore';
import {animate, keyframes, query, stagger, style, transition, trigger} from '@angular/animations';


export interface Group {
  name: string;
  players: DocumentReference[];
  results: object;
  rules?: any[];
  author?: string;
  ref?: DocumentReference;
}

export interface Round {
  id: string;
  name: string;
  groups: Group[];
  groupsCol: DocumentReference[];
  scoring: object;
  timing: string;
  notes: string;
}

export interface Edition {
  scoring: object;
  id: string;
  name: String;
  rounds: Round[];
  scoreboard: object[];
}

export interface League {
  id: string;
  name: string;
  edition: Edition[];
  visible: boolean;
}

export interface Player {
  id: string;
  name: string;
  email: string;
}

export class Result {
  date: Date;
  group: Group;
  player: DocumentReference;
  opponent: DocumentReference;
  score: string;

  constructor(group: Group, player: DocumentReference, opponent: DocumentReference, score: string) {
    this.group = group;
    this.player = player;
    this.opponent = opponent;
    this.score = score;
    this.date = new Date();
  }
}

@Component({
  selector: 'app-leagues',
  templateUrl: './leagues.component.html',
  styleUrls: ['./leagues.component.css'],
  animations: [
    trigger('ngIfAnimation', [
      transition('void => *', [
        query('.edition', stagger('300ms', [
          animate('0.8s ease-in', keyframes([
            style({opacity: 0, transform: 'translateY(-75%)', offset: 0}),
            style({opacity: .5, transform: 'translateY(35px)', offset: 0.3}),
            style({opacity: 1, transform: 'translateY(0)', offset: 1.0}),
          ]))]), {optional: true}),
      ]),
      transition('* => void', [
        query('*', style({opacity: 1}), {optional: true}),
        query('.edition', stagger('300ms', [
          animate('0.8s ease-in', keyframes([
            style({opacity: 1, transform: 'translateY(0)', offset: 0}),
            style({opacity: .5, transform: 'translateY(35px)', offset: 0.3}),
            style({opacity: 0, transform: 'translateY(-75%)', offset: 1.0}),
          ]))]), {optional: true}),
      ])
    ])
  ]
})
export class LeaguesComponent implements OnInit {

  leagues: Observable<League[]>;

  constructor(private fs: FirestoreService) {
  }

  ngOnInit() {
    this.leagues = this.fs.collectionWithIds<League>('leagues', ref => ref.orderBy('name', 'desc'));
  }

  private dumpLeagues(obj) {
    let cache = [];
    const serialized = JSON.stringify(obj, function (key, value) {
      if (['firestore', '_firestoreClient'].indexOf(key) >= 0) {
        return;
      }
      if (key === '_key') {
        return value['path']['segments'].join('/');
      }
      if (typeof value === 'object' && value !== null) {
        if (cache.indexOf(value) !== -1) {
          // Duplicate reference found
          try {
            // If this value does not reference a parent it can be deduped
            return JSON.parse(JSON.stringify(value));
          } catch (error) {
            // discard key if value cannot be deduped
            return;
          }
        }
        // Store value in our collection
        cache.push(value);
      }
      return value;
    });
    cache = null; // Enable garbage collection
    console.log(serialized);
  }

}

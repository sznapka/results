import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {FirestoreService} from '../../services/firestore.service';
import {Edition, Group, League, Result, Round} from '../leagues/leagues.component';
import {AngularFireAuth} from 'angularfire2/auth';
import {User} from 'firebase';
import {ScoringService} from '../../services/scoring.service';
import {DocumentReference} from 'angularfire2/firestore';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {PlayersComponent} from '../../modal/players/players.component';
import {SettleRoundComponent} from '../settle-round/settle-round.component';

@Component({
  selector: 'app-round',
  templateUrl: './round.component.html',
  styleUrls: ['./round.component.css']
})
export class RoundComponent implements OnInit {

  public user: User;
  public league: League;
  public edition: Edition;
  public round: Round;
  public prevRound: Round;
  public nextRound: Round;
  public groups = {};
  public addScoreVisible = {};
  public results = {};
  public resultsCount = 0;
  public playersCount = 0;
  public editMode = false;

  constructor(protected fs: FirestoreService, protected route: ActivatedRoute, public afAuth: AngularFireAuth, private modal: NgbModal) {
  }

  ngOnInit() {
    this.afAuth.authState.subscribe(res => this.user = res);
    this.route.paramMap.subscribe((params) => {
      this.league = this.edition = this.round = this.prevRound = this.nextRound = this.results = null;
      this.groups = this.addScoreVisible = {};
      this.editMode = false;

      this.fs.doc<League>('leagues/' + params.get('id')).subscribe((res: League) => {
        this.edition = res.edition.filter(x => x.name === params.get('edition'))[0];
        this.league = {id: params.get('id'), ...res};
        this.edition.rounds.forEach((round, index) => {
          if (round.name === params.get('round')) {
            this.round = round;
            if (index - 1 in this.edition.rounds) {
              this.prevRound = this.edition.rounds[index - 1];
            }
            if (index + 1 in this.edition.rounds) {
              this.nextRound = this.edition.rounds[index + 1];
            }
          }
        });
        for (const group of this.round.groupsCol) {
          this.getResults(group);
        }
      });
    });
  }

  range(n: number): any[] {
    return Array(n);
  }

  getResults(group: DocumentReference) {
    this.resultsCount = 0;
    this.groups = {};
    this.fs.collectionWithIds('results', r => r.where('group', '==', group))
      .subscribe((results: Result[]) => {
        if (!this.results) {
          this.results = {};
        }
        for (const result of results) {
          this.results[`${result.player.path}:${result.opponent.path}`] = result.score;
        }
        this.resultsCount = Object.keys(this.results).length;
        group.get().then(r => {
          const groupObject: Group = ScoringService.calculatePoints(r.data() as Group, this.results,
            this.round.scoring || this.edition.scoring);

          groupObject.ref = group;
          if (!('rules' in groupObject)) {
            groupObject.rules = Array(groupObject.players.length).fill(0, 0);
          }
          if (groupObject.rules.length < groupObject.players.length) {
            console.log('filling');
            groupObject.rules = [...groupObject.rules, ...Array(groupObject.players.length - groupObject.rules.length).fill(0, 0)];
          }
          this.groups[groupObject.name] = groupObject;
          this.playersCount = 0;
          for (const g of Object.values(this.groups)) {
            this.playersCount += (g as Group).players.length;
          }
        });
      });
  }

  addGroup() {
    const lastGroup = Object.values(this.groups)[Object.values(this.groups).length - 1] as Group;
    const lastGroupName = lastGroup ? lastGroup.name : 'Grupa 0';
    const lastGroupNameParts = lastGroupName.split(' ');
    const lastGroupNumber = parseInt(lastGroupNameParts[lastGroupNameParts.length - 1], 0);
    lastGroupNameParts[lastGroupNameParts.length - 1] = '' + (lastGroupNumber + 1);
    const name = prompt('Nazwa grupy', lastGroupNameParts.join(' '));
    if (name) {
      this.fs.add('groups', {name: name, players: []}).then(res => {
        this.round.groupsCol.push(res);
        this.fs.update('leagues/' + this.league.id, this.league);
      });
    }
  }

  assignPlayerModal(targetGroup: Group) {
    const modalRef = this.modal.open(PlayersComponent);
    const usedPlayers = [];
    const players = [];
    modalRef.componentInstance.group = targetGroup;
    for (const group of Object.values(this.groups)) {
      usedPlayers.push(...group['players'].map(ref => ref['path'].replace('players/', '')));
    }
    this.fs.collectionWithIds('players').subscribe(res => {
      for (const player of res) {
        if (!usedPlayers.includes(player['id'])) {
          players.push(player);
        }
      }
      modalRef.componentInstance.players = players;
    });
  }

  removePlayer(playerName: string, player: DocumentReference, group: Group) {
    if (window.confirm(`Na pewno usunąć gracza ${playerName} z grupy ${group.name}?`)) {
      group.players = group.players.filter(x => x.path !== player.path);

      const ref = group['ref'];
      delete group['ref'];
      this.fs.update(ref.path, group).then(() => group['ref'] = ref);
    }
  }

  changeRules(group: Group, playerIdx, number: number) {
    if (!group['rules']) {
      group['rules'] = [];
    }
    group['rules'][playerIdx] = number;
    const ref = group['ref'];
    delete group['ref'];
    this.fs.update(ref.path, group).then(() => group['ref'] = ref);
  }

  settleRoundModal() {
    const modalRef = this.modal.open(SettleRoundComponent);
    modalRef.componentInstance.league = this.league;
    modalRef.componentInstance.edition = this.edition;
    modalRef.componentInstance.round = this.round;
    modalRef.componentInstance.groups = this.groups;
    modalRef.componentInstance.init();

    modalRef.result.then(newRoundName => {
      console.log(newRoundName);
      window.location.href = ['/league', this.league.id, 'edition', this.edition.name, 'round', newRoundName].join('/');
    });
  }

  saveField(field, value) {
    this.round[field] = value;
    this.fs.update('leagues/' + this.league.id, this.league).then(() => this.editMode = false);
  }
}

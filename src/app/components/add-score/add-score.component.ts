import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Group} from '../leagues/leagues.component';
import {AngularFirestore, DocumentReference} from 'angularfire2/firestore';
import {AngularFireAuth} from 'angularfire2/auth';
import {User} from 'firebase';

@Component({
  selector: 'app-add-score',
  templateUrl: './add-score.component.html',
  styleUrls: ['./add-score.component.css']
})
export class AddScoreComponent {

  @Input() group: Group;
  @Input() results: object;
  @Input() scoring: object;
  @Output() canceled: EventEmitter<boolean> = new EventEmitter<boolean>();

  public player: DocumentReference;
  public opponent: DocumentReference;
  public score: string;
  public opponents: DocumentReference[] = [];
  public saving = false;

  constructor(private afs: AngularFirestore, private afAuth: AngularFireAuth) {
  }

  cancel() {
    this.canceled.emit(true);
  }

  playerChanged(player: DocumentReference) {
    const opponents = [];
    for (const match of Object.keys(this.results).filter(x => x.indexOf(player.path) > -1)) {
      opponents.push(match.replace(player.path, '').replace(':', ''));
    }
    this.opponents = this.group.players.filter(x => !opponents.includes(x.path) && x.path !== player.path);
  }

  save(score: string) {
    if (this.saving) {
      return;
    }
    this.saving = true;
    this.afAuth.user.subscribe((user: User) => {
      this.afs.collection('results').add({
        group: this.group.ref,
        player: this.player,
        opponent: this.opponent,
        score: score,
        date: new Date(),
        author: user.email,
      }).then(() => this.saving = false, () => this.saving = false);
    });
  }
}

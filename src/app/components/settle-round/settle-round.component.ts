import {Component} from '@angular/core';
import {Edition, Group, League, Round} from '../leagues/leagues.component';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {FirestoreService} from '../../services/firestore.service';

@Component({
  selector: 'app-settle-round',
  templateUrl: './settle-round.component.html',
  styleUrls: ['./settle-round.component.css']
})
export class SettleRoundComponent {

  public newGroups = {};
  public league: League;
  public edition: Edition;
  public round: Round;
  public groups = {};

  readonly maxPoints = 100;

  constructor(public activeModal: NgbActiveModal, protected fs: FirestoreService) {
  }

  public init() {
    const newGroupsCol = Object.keys(this.groups).map(name => {
      return {
        'name': name,
        'players': [],
        'rules': this.groups[name].rules,
      };
    });

    let points = this.maxPoints;
    Object.keys(this.groups).forEach((groupName, groupOrder) => {
      const group: Group = this.groups[groupName];
      if (groupOrder > 0) {
        points += 3;
      }
      group.players.forEach((player, place) => {
        player['score'] = (player['matches'] > 0 && player['walkovers'] < player['matches']) ? points : 0;
        if (group.rules[place] === 0) {
          player['movement'] = 0;
          newGroupsCol[groupOrder].players.push(player);
        }
        if (group.rules[place] === 1) {
          player['movement'] = 1;
          newGroupsCol[groupOrder - 1].players.push(player);
        }
        if (group.rules[place] === -1) {
          player['movement'] = -1;
          newGroupsCol[groupOrder + 1].players.push(player);
        }
        points -= 3;
      });
    });

    newGroupsCol.filter(x => x.players.length > 0).forEach(newGroup => this.newGroups[newGroup.name] = newGroup);
  }

  startRound() {
    const lastRoundNameParts = this.round.name.split(' ');
    const lastRoundNumber = parseInt(lastRoundNameParts[lastRoundNameParts.length - 1], 0);
    lastRoundNameParts[lastRoundNameParts.length - 1] = '' + (lastRoundNumber + 1);
    const name = prompt('Nazwa rundy', lastRoundNameParts.join(' '));
    if (name) {
      this.edition.scoreboard.push(this.getScores(this.round, this.newGroups));
      const round = {
        'name': name,
        groupsCol: [],
      };
      let groupsAddedCounter = 0;
      for (const newGroup of Object.values(this.newGroups)) {
        this.fs.add('groups', newGroup).then(res => {
          round.groupsCol.push(res);
          if (Object.keys(this.newGroups).length === ++groupsAddedCounter) {
            this.league.edition.filter(x => x.name === this.edition.name)[0].rounds.push(round as Round);
            this.fs.update('leagues/' + this.league.id, this.league).then(() => this.activeModal.close(name));
          }
        });
      }
    }
  }

  private getScores(round: Round, newGroups: {}) {
    const points = {};
    Object.values(newGroups).flatMap((group: Group) => group.players).forEach(playerRef => points[playerRef.id] = playerRef['score']);
    return {
      round: round.name,
      points: points,
    };
  }
}

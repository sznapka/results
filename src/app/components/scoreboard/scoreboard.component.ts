import {Component, OnInit} from '@angular/core';
import {FirestoreService} from '../../services/firestore.service';
import {ActivatedRoute} from '@angular/router';
import {Edition, League, Round} from '../leagues/leagues.component';

@Component({
  selector: 'app-scoreboard',
  templateUrl: './scoreboard.component.html',
  styleUrls: ['./scoreboard.component.css']
})
export class ScoreboardComponent implements OnInit {

  public players: any[];
  public league: League;
  public edition: Edition;
  public round: Round;

  constructor(private fs: FirestoreService, protected route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe((params) => {
      const players = {};
      this.fs.doc<League>('leagues/' + params.get('id')).subscribe((res: League) => {
        this.league = res;
        this.edition = res.edition.filter(x => x.name === params.get('edition'))[0];
        this.round = this.edition.rounds[this.edition.rounds.length - 1];
        this.edition.scoreboard.forEach(round => {
          for (const player of Object.keys(round['points'])) {
            const key = `players/${player}`;
            if (!(key in players)) {
              players[key] = 0;
            }
            players[key] += round['points'][player];
          }
        });

        // @ts-ignore
        this.players = Object.entries(players).sort((a, b) => b[1] - a[1]);
      });

    });
  }

}

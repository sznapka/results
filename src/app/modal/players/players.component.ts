import {Component, OnInit} from '@angular/core';
import {Group} from '../../components/leagues/leagues.component';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {FirestoreService} from '../../services/firestore.service';

@Component({
  selector: 'app-players',
  templateUrl: './players.component.html',
  styleUrls: ['./players.component.css']
})
export class PlayersComponent implements OnInit {

  public group: Group;
  public selectedPlayers = {};
  public players: any[];
  public newPlayers = [1];
  public newPlayersValues = {};

  constructor(public activeModal: NgbActiveModal, private fs: FirestoreService) {
  }

  ngOnInit() {
  }

  save() {
    this.activeModal.dismiss();
    const newPlayers = Object.keys(this.selectedPlayers)
      .filter(x => this.selectedPlayers[x])
      .map(x => this.fs.collection('players').doc(x).ref);
    const ref = this.group['ref'];
    delete this.group['ref'];

    this.group.players.push(...newPlayers);
    this.fs.update(ref.path, this.group).then(() => this.group['ref'] = ref);

    const freshPlayers = Object.values(this.newPlayersValues) as string[];
    if (freshPlayers.length > 0) {
      for (const name of freshPlayers) {
        let newPath: string = null;
        if (name.indexOf(' ') === -1) {
          newPath = name;
        } else {
          const nameParts = name.split(' ');
          newPath = nameParts[0][0] + nameParts[1];
        }
        newPath = newPath.toLowerCase();
        this.fs.collection('players').doc(newPath).set({name: name}).then(() => {
          this.group.players.push(this.fs.collection('players').doc(newPath).ref);
          this.fs.update(ref.path, this.group);
        });
      }
    }
  }

  appendNewPlayer() {
    this.newPlayers.push(this.newPlayers[this.newPlayers.length - 1] + 1);
  }

  removeNewPlayer(i: number) {
    delete this.newPlayersValues[i];
    this.newPlayers.splice(this.newPlayers.indexOf(i), 1);
  }
}

import {Pipe, PipeTransform} from '@angular/core';
import {FirestoreService} from '../services/firestore.service';
import {Observable} from 'rxjs';

@Pipe({
  name: 'doc'
})
export class DocPipe implements PipeTransform {

  constructor(private db: FirestoreService) {
  }

  transform(value: any, args?: any): Observable<any> {
    return this.db.doc(typeof value === 'string' ? value : value.path) as Observable<any>;
  }

}

import {Pipe, PipeTransform} from '@angular/core';
import {Group} from '../components/leagues/leagues.component';

@Pipe({
  name: 'groupAbbrev'
})
export class GroupAbbrevPipe implements PipeTransform {

  transform(group: Group, args?: any): any {
    if (group) {
      const parts = group.name.split(' ');
      if (parts.length === 1) {
        return group.name;
      } else {
        return parts[1];
      }
    }
    return null;
  }

}

import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'shorten'
})
export class ShortenPipe implements PipeTransform {

  transform(value: string, args?: any): any {
    if (value) {
      const parts = value.split(' ');
      return parts[0].substr(0, 1) + parts[1].substr(0, 1);
    }
  }

}

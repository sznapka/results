import {Pipe, PipeTransform} from '@angular/core';
import {Player} from '../components/leagues/leagues.component';

@Pipe({
  name: 'surname'
})
export class SurnamePipe implements PipeTransform {

  transform(player: Player, args?: any): any {
    if (player) {
      return player.name.split(' ')[1];
    }
    return null;
  }

}

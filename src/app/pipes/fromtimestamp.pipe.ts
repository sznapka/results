import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'fromtimestamp'
})
export class FromtimestampPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (value) {
      return new Date(value.seconds * 1000);
    }
  }

}

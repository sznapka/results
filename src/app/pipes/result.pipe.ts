import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'result'
})
export class ResultPipe implements PipeTransform {

  transform(player: any, args?: any): any {
    const results = args[0];
    let result = null;
    if (results) {
      const opponent = args[1];
      const key = `${player}:${opponent}`;
      const reverseKey = `${opponent}:${player}`;

      if (key in results) {
        result = results[key];
      }

      if (reverseKey in results) {
        if (results[reverseKey].indexOf(':') > 0) {
          const parts = results[reverseKey].split(':');
          result = `${parts[1]}:${parts[0]}`;
        } else if (results[reverseKey] === 'wo') {
          result = 'wo';
        } else if (results[reverseKey] === 'wo+') {
          result = 'wo-';
        } else if (results[reverseKey] === 'wo-') {
          result = 'wo+';
        } else {
          console.error('No idea how to treat value ' + results[reverseKey]);
          result = null;
        }
      }
    }
    if (args.length === 3 && args[2] && result) {
      return 'score' + result.replace(':', '').replace('+', 'plus').replace('-', 'minus');
    } else {
      return result;
    }
  }

}

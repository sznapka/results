export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyC8BbvOolEcnBYl5XSwhcFovvq9ypBsHvc',
    authDomain: 'results-ba098.firebaseapp.com',
    databaseURL: 'https://results-ba098.firebaseio.com',
    projectId: 'results-ba098',
    storageBucket: 'results-ba098.appspot.com',
    messagingSenderId: '995376764807'
  }
};
